+++
title = "Linux Terminal Emulator Features and Hardware Compatibility"
author = ["Benjamin Slade"]
date = 2022-06-28T16:57:00+05:45
categories = ["terminals"]
draft = false
creator = "Emacs 28.1 (Org mode 9.5.2 + ox-hugo)"
+++

In a continuing series of, er, terminal-related posts, a look at some
features of a subset of terminal emulators on Linux.

I mainly use [vterm inside of Emacs](https://github.com/akermu/emacs-libvterm), usually via [Equake](https://babbagefiles.xyz/categories/equake/), but sometimes
I do want to spawn a terminal outside of Emacs, and so I've been
curious about the properties of different terminals, including the
ability to be used across a wide range of hardware.

A few weeks ago, I came across [Zutty](https://tomscii.sig7.se/zutty/), which describes itself as:

> Zutty is a terminal emulator for the X Window System, functionally similar to several other X terminal emulators such as xterm, rxvt and countless others. It is also similar to other, much more modern, GPU-accelerated terminal emulators such as Alacritty and Kitty. What really sets Zutty apart is its radically simple, yet extremely efficient rendering implementation, coupled with a sufficiently complete feature set to make it useful for a wide range of users. Zutty offers high throughput with low latency, and strives to conform to relevant (published or de-facto) standards.
>
> Zutty is written in straightforward C++ and only relies on OpenGL ES 3.1 for rendering...

The Zutty page includes a link to a blog post ["A totally biased comparison of Zutty (to some better-known X terminal emulators)"](https://tomscii.sig7.se/2020/12/A-totally-biased-comparison-of-Zutty),
which includes some interesting discussion of features of different
terminal emulators, including comparison of VT support levels, and
discussion of why this could be important. A number of terminal
emulators, perhaps leaning towards "minimalism", only implement
support up to VT220, or even lower, which creates inefficiencies in
certain cases:

> The difference between implementing a VT400 or VT500 terminal (provided the implementations are correct) is relatively inconsequential. The same cannot be said of the gap between these and VT220-level terminals. Programs running in a less capable virtual terminal must sometimes use longer series of basic escape sequences to achieve the results of a fewer number of more modern ones. For example, the DECLRMM control sequence (set left-right margin mode) is available from the VT420 and up, and will be used to restrict scrolling to the active part of two horizontally split tmux panes. On less capable terminals, tmux is forced to perform more work to achieve the same result. This is quite similar to how extended instructions on modern CPUs allow machine code to be more efficient than code compiled for an older machine. In other words, not implementing a modern VT variant goes directly counter to greater efficiency. And there is yet another downgrade from VT220 to those terminals that only claim to support VT100/VT102.
>
> It is surprising how unambitious the newer terminals Alacritty and Kitty are in this regard. I would have hoped that new, supposedly state-of-the-art entrants would take the effort to add support for modern VT standards. Switching from xterm to Alacritty (or any other modern terminal) should not be a major downgrade.

Inspired by this, I copy/supplement the table on that page with some
additional comparison information, and a few additional terminal
emulators. As per the Zutty post, I use [VTTEST](https://invisible-island.net/vttest/) (originally written in
1983-1985 by Per Lindberg at the Stockholm University Computing
Center; in 1996 a new version written by Thomas E. Dickey, the
maintainer of [xterm](https://en.wikipedia.org/wiki/Xterm)). to check for reported
VT support level. The below table includes also whether the terminal
emulator is GPU-accelerated or not, and what the mininum [OpenGL](https://en.wikipedia.org/wiki/OpenGL)
hardware support is, as well as what display server is targeted ([X11](https://en.wikipedia.org/wiki/X11) or
[Wayland](https://en.wikipedia.org/wiki/Wayland_(display_server_protocol)) or both).

| program        | tested version | VT self-id  | GPU-accel     | minimum OpenGL | DS      |
|----------------|----------------|-------------|---------------|----------------|---------|
| gnome-terminal | 3.30.2         | VT525       | no?           | n/a            | both    |
| zutty          | 0.6            | VT520       | yes           | ES 3.1         | X11     |
| terminology    | 1.12.1         | VT510       | when possible | n/a            | X11     |
| wezterm        | 20220624       | VT500       | yes           | 3.3 (?)        | both    |
| xterm          | 344            | VT420       | no            | n/a            | X11     |
| kitty          | 0.19.3         | VT220       | yes           | 3.3            | both    |
| foot           | 1.12.1         | VT220       | no            | n/a            | wayland |
| alacritty      | 0.4.3          | VT102       | yes           | 3.3            | both    |
| st             | 0.8.2          | VT102       | no            | n/a            | X11     |
| urxt           | v9.22          | VT102       | no            | n/a            | X11     |
| (lib)vterm     | 0.1.4          | VT100/VT102 | n/a           | n/a            | n/a     |

[Foot](https://codeberg.org/dnkl/foot) is another interesting new terminal emulator - targetting Wayland
only, and not GPU-accelerated, but with some design choices that make
it faster than GPU-accelerated terminal emulators in some cases, as
described in the wiki ["When is foot fast, and when is it not?"](https://codeberg.org/dnkl/foot/wiki/Performance). One of
the things it does is "damage tracking", i.e. only rendering cells
that have been updated, whereas e.g. alacritty rerenders everything
(though, as noted, "Alacritty renders empty cells really fast"). But
foot only supports up to VT220.

The terminal emulator I end up using most is based on [libvterm](https://www.leonerd.org.uk/code/libvterm/), this
is the library underlying Emacs' vterm, and claims to "implement[]
a VT220 or xterm-like terminal emulator", but VTTEST seems to report
as VT102/VT100, at least inside of Emacs.

There's no clear "best choice" here, even putting vterm aside (vterm
isn't particularly fast, at least inside of Emacs (though it's faster
than other Emacs terminal emulator choices like `ansi-term`), doesn't
have "very high" VT support; but it runs inside in Emacs, which offers
me a lot of advantages). Though alacritty, [st](https://st.suckless.org/), and [urxvt](https://en.wikipedia.org/wiki/Rxvt) implement
only up to VT102 level features.

[Foot](https://codeberg.org/dnkl/foot), [Zutty](https://tomscii.sig7.se/zutty/), [Kitty](https://sw.kovidgoyal.net/kitty/), and [Alacritty](https://github.com/alacritty/alacritty) are all fast, at least sometimes (the Zutty
biased comparison post above offers some additional discussion on
this, not including foot). They're all limited in various ways
(hardware or display server - though Zutty apparently has [experimental
support for software rendering](https://github.com/tomszilagyi/zutty/wiki/FAQ)), with only
Zutty having "very high" VT support. (And Zutty has some
[bells-and-whistles limitations](https://github.com/tomszilagyi/zutty/wiki/FAQ): no plans for transparency support,
ligatures, bitmap images (e.g. [SIXEL](https://en.wikipedia.org/wiki/Sixel)).)

Foot runs on pretty much any hardware and has some nice features, but
is Wayland only (and limited in VT level support).

Kitty and Alacritty require OpenGL 3.3 (on Intel hardware, this means
at least Intel HD Graphics 3000 (so 2011-era \*\*20 ThinkPads or later)).

Zutty "runs on a wider range of graphics hardware by virtue of only
requiring [OpenGL ES](https://en.wikipedia.org/wiki/OpenGL_ES) as opposed to “desktop” OpenGL, and its resource
demands are otherwise minimal", but it requires ES 3.1, which isn't
supported by, for instance, Intel HD Graphics 3000.

[Terminology](https://github.com/borisfaure/terminology), a terminal emulator which doesn't seem to get talked
about as much, actually turns out to be a good choice across different
hardware. It can use GPU-acceleration, rendering using OpenGL or
OpenGL-ES2, but this is not a hard requirement. It offers VT510 level
support, and has lots of bells and whistles. I haven't seen many speed
test comparisons with it, but in use it feels fast. (No native Wayland
support though, if that matters to you.)

[Edit: Added [Wezterm](https://wezfurlong.org/wezterm/) (another one written in Rust) which I haven't
really had a chance to play with much, but it has good VT feature
levels. It seems to have the same OpenGL requirements as Kitty or Alacritty.]

Still, I'm likely to be mainly using [vterm](https://github.com/akermu/emacs-libvterm) and [eshell](https://www.masteringemacs.org/article/complete-guide-mastering-eshell). Because [Emacs](https://babbagefiles.xyz/categories/emacs/).

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"