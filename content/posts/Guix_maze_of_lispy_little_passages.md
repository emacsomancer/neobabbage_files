+++
title = "Guix: You are in a maze of lispy little passages, (map equal? ′(′all ′alike) ′(′all ′alike))"
author = ["Benjamin Slade"]
date = 2018-08-04T21:47:00-05:00
categories = ["lisp", "guix", "linux", "stumpwm", "emacs"]
draft = false
creator = "Emacs 30.0.93 (Org mode 9.8-pre + ox-hugo)"
+++

So I finally made a serious go of running [GuixSD](https://www.gnu.org/software/guix/), a GNU Linux distro
which is largely built on [GNU Guile Scheme](https://en.wikipedia.org/wiki/GNU_Guile) (a dialect of Lisp) on one
of my machines (one I had actually put together with GuixSD in mind:
an X200 Thinkpad, which I [Libreboot](https://libreboot.org/)'ed and put a Atheros Wi-Fi card
in), and, to increase both the quantity and variety of Lisps involved,
am trying to use with [StumpWM](https://stumpwm.github.io/) (which is written in Common Lisp).

It's a fascinating distro, modelled on [Nix](https://nixos.org/), but implemented in
Guile. It's not been exactly easy to get running ([one of the videos on
GuixSD from Fosdem 2017](https://www.gnu.org/software/guix/blog/2017/back-from-fosdem-2017/) included the line "[GuixSD] is like Gentoo for
grown ups"), in part because its architecture is rather different from what
I've experienced with other Linux distros, which use different package
managers perhaps and sometimes even different libc's, but generally
follow a similar design philosophy. Rather than spreading out
configuration across lots of different pieces, GuixSD seems to largely
try to concentrate it in specific configuration files which are
transactional in that they can be rolled back (and thus the system can
be rolled back to known working states).

It is a GNU-blessed distro, and does take the FSF's hard line (and to
my eyes sometimes weird line) approach to software. So no proprietary
software is included in the Guix repos, including firmware (and it
runs on the linux-libre kernel). That by itself is fine, but it means
the state of affairs for Guix-packaged browsers is pretty poor. No
Chromium, no Firefox. IceCat 52 is essentially what's currently
available (if IceCat were up to the latest Firefox ESR 60, it might be
easier) in terms of browsers which might be considered secure.

This led me to try to use the Nix installer by itself<sup>[\*](#org-target--0b)</sup><span class="org-target" id="org-target--0t"></span> to try to
install Firefox and Chromium. Sadly, I can't get Nix's Chromium to
work at all on GuixSD, and while Firefox works fine, I can't get it to
see my locally installed fonts (or other fonts I've installed via
Nix).

Hopefully at some point [Next Browser](http://next-browser.com/) will be packaged for Guix, to
bring in another major component written in (Common) Lisp. And when
(if?) IceCat 60 comes out, that will alleviate the pain somewhat. (I
was a long-time [Conkeror](http://conkeror.org/) user, and I briefly tried it again in GuixSD,
but I'm not certain of its security and uBlock Origin no long works
with it, which I believe is why I stopped using it in the first
place).

Other interesting Lispy pieces include [mcron](https://www.gnu.org/software/mcron/), a cron which accepts (as
well as Vixie cron style, I think) Guile config files. The [examples in
Guix manual](https://www.gnu.org/software/guix/manual/en/html_node/Scheduled-Job-Execution.html) I couldn't really get to work. But via the `help-guix`
listserv I found that one can put simple guile scripts in
`~/.config/cron/job.guile`. Working out how to do a 'run this every N
minutes' was not immediately obvious, but I figured out how to do it,
e.g.:

```guile
; execute run_me every 5 minutes
(job '(next-minute (range 0 60 5)) "run_me")
; run execute_me every 2 hours
(job '(next-hour (range 0 24 2)) "execute_me")
```

One of the other great things about GuixSD is that its init manager,
[GNU Shepherd](https://www.gnu.org/software/shepherd/), is also written in Guile Scheme. I've only had a chance
to play with it a little bit, but it seems very nice and it's good to
find other innovative init managers (I would mention here also [runit](http://smarden.org/runit/)
and [s6](https://skarnet.org/software/s6/)) which take very different approaches to [systemd](https://www.freedesktop.org/wiki/Software/systemd/) (another
innovative init, or perhaps init+, but one that creates more problems
than it solves in the end, in my experience).

On the Guix package manager itself: I learned the hard way that
searching for packages in Guix is really only comfortable within
Emacs: so do `guix package -i guix-emacs` and then do everything else
from guix-emacs within Emacs ( `M-x guix-search-by-name` to search
package names by regex; and `M-x guix-search-by-regex` to search
names+descriptions by regex). The results returned by `guix package -s
....` in a terminal are not very browseable (though I tried valiantly
for some time). But if you're interested in Guix, you'll likely
interested in Emacs anyway.

What I'm trying to build on this machine is something with lots of
Lisp. Of course the kernel is still a kernel written in C, as are lots
of the other pieces like the terminal &amp;c., but much of the user-facing
things: the package manager, the windows manager, the init, the job
scheduler (=cron), and (most importantly perhaps) the 'text editor'
(read: document composer, email interface, irc interface, twitter
interface, blog post interface, code editor ...) are all largely
written in and interacted with using some form of Lisp (Guile Scheme,
Common Lisp, Emacs Lisp).

Guix is a bit like Emacs, I think. It's an incredibly powerful tool
with lots of interesting possibilities, but when you start using it
you're presented with an empty screen with little indication of what
you can do. I'll be sticking with it, I think. Now I've got to get to
grips with StumpWM and figure out how to configure [polybar](https://github.com/jaagr/polybar)...

[And if you're curious about why Guix is pronounced like "geeks", have
a look at [this post over on my linguistics blog](https://slade.jnanam.net/post/scheming-french-geeks-with-guile/).]

[\*](#org-target--0t)<span class="org-target" id="org-target--0b"></span> As well as being the package managers of both of their
respective distros, both the Nix and Guix package managers can be used
**on top of** other distros. Nix doesn't have quite the same hard line
approach to software licences as Guix.


[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
