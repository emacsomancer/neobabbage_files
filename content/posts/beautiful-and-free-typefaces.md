+++
title = "Beautiful and Free Unicode Typefaces, for editor and printer (including a comparison of Latin Modern and Computer Modern Unicode)"
author = ["Benjamin Slade"]
date = 2019-08-04T02:25:00-06:00
images = ["images/01-diacritictests.png", "images/02-sentencetests.png", "images/03-serifparas.png", "images/04-sansparas.png", "images/05-monoparas.png", "images/06-winners.png", "images/cmutypewriter-emacs.png", "images/dejavusansmono-emacs.png", "images/iosevkaoakvise-emacs.png", "images/notosansmono-emacs.png", "images/dunhill-computermodernconcrete-faces-lambdacalculusslide.png"]
tags = ["17th-century", "knuth"]
categories = ["typography", "iosevka", "computermodern", "emacs", "latex"]
draft = false
creator = "Emacs 26.3 (Org mode 9.3.7 + ox-hugo)"
+++

For my academic papers, I often need a typeface with a wide range of
characters and diacritic combinations. Basic diacritics are supported
by a wide range of fonts, but more specialised diacritics and
particularly combinations of diacritics only work well in a handful of
typefaces. I write my papers in TeX, which has two components: the
typeface used to set the paper in (La)TeX and the typeface/font used
inside Emacs, where I write the papers.


## Typeset typefaces {#typeset-typefaces}

The choice of typeface is particularly relevant for using XeLaTeX or
LuaLaTeX, where the standard default unicode typeface family is [Latin
Modern](https://en.wikipedia.org/wiki/Computer%5Fmodern#Latin%5FModern), based on [Donald Knuth](https://en.wikipedia.org/wiki/Donald%5Fknuth)'s original [Computer Modern](https://en.wikipedia.org/wiki/Computer%5FModern) family of
typefaces designed in [METAFONT](https://en.wikipedia.org/wiki/Metafont), and the general recommendation is to
use Latin Modern rather than the earlier [Computer Modern Unicode](http://cm-unicode.sourceforge.net/).

Generally reasons given for preferring Latin Modern include the fact
that Latin Modern involves handmade vectorisation, with revised
metrics, and additional glyph coverage, including particularly
diacritic characters; whereas the Computer Modern Unicode fonts were
converted from METAFONT sources using [`mftrace`](http://lilypond.org/mftrace/) with [`autotrace`](http://autotrace.sf.net/) backend
and [`fontforge`](http://fontforge.sf.net/) (former pfaedit) automatically.

Some of the revised metrics of Latin Modern include arguably better
positioning of acute and grave accents, more appropriately-sized
umlauts, and (here tastes may vary) a better looking Eszett (ß).

**[Note that throughout this post, you can right-click on the images and
select "View Image" to see a larger/zoomable version of the image.]**

However, in my tests, Computer Modern Unicode actually performs better
at least with respect to handling diacritics. In fact, Computer Modern
Unicode is one of a handful of typefaces which actually properly
handle a large range of diacritics and combinations of diacritics, as
can be seen in the image below, showing a variety of serif, sans
serif, and monospace typefaces, respectively, typeset in XeLaTeX:

{{< figure src="/ox-hugo/01-diacritictests.png" >}}

[The source file from which all of these samples were generated is
included below in this footnote:&nbsp;[^fn:1].]

Note that in many typefaces, including all of the Latin Modern faces,
combinations of acute accents and macrons (e.g. the first character in
the tests: **ā́**, also not handled well by [IM Fell](https://iginomarini.com/fell/) Great Primer, the font
this blog _used to use_ for its body text; now switched to Computer
Modern Unicode Sans) are very poorly handled, often with the accent and
macron being overlaid, rather than the accent properly stacking on top
of the macron.

Even fonts that properly handle this often fail for other diacritics:
note that [EB Garamond](http://www.georgduffner.at/ebgaramond/) and [Liberation Serif](https://en.wikipedia.org/wiki/Liberation%5Ffonts) don't seem to properly
handle underrings (e.g. **R̥r̥**, also not handled that well by IM Fell Great
Primer).

Even the [Linux Libertine](http://libertine-fonts.org/) faces (here represented by their [Libertinus
continuations](https://github.com/alif-type/libertinus)), which overall fare well, don't handle the acute+macron
combination well in the italic version _**ā́**_.

Thus the only serif faces which handle the full range of
characters/diacritics are the [didone](https://en.wikipedia.org/wiki/Didone%5F(typography)) (a style arising in the
late 18th-century, popular in the 19th-c.) Computer Modern Unicode;
[Junicode](https://en.wikipedia.org/wiki/Junicode) (based on a 17th-century font used for [George Hickes](https://en.wikipedia.org/wiki/George%5FHickes%5F(divine))'s
_Linguarum Vett. Septentrionalium Thesaurus_); and the early
20th-century styled Times New Romanesque [Noto Serif](https://en.wikipedia.org/wiki/Noto%5Ffonts).

For the sans serif faces, in addition to the Computer Modern Unicode
Sans Serif face (in some ways a 'skeletal', un-didone-ised version of
Computer Modern Roman), Mozilla's [Fira Sans](https://en.wikipedia.org/wiki/Fira%5FSans) (here represented by
[FiraGO](https://bboxtype.com/typefaces/FiraGO/#!layout=specimen), an updated/extended version), and [Noto Sans](https://fonts.google.com/specimen/Noto+Sans) – the latter two
both being vaguely ['humanist'](https://en.wikipedia.org/wiki/Sans-serif#Humanist) sans serifs, though Noto Sans perhaps
has some ['american gothic/grotesque'](https://en.wikipedia.org/wiki/Sans-serif#Grotesque) features – also manage the full
range of characters/diacritics, but [doesn't have an italic face](https://github.com/googlefonts/noto-fonts/issues/1376).

For the monospace faces, again the Computer Modern Typewriter face (an
['Egyptian'](https://en.wikipedia.org/wiki/Slab%5Fserif) typewriter face, not unlike Courier) manages the full range
of characters/diacritics, as do Noto Sans Mono and the [Iosevka](https://typeof.net/Iosevka/) faces
(here represented by my own customised version, Iosevka Oak Vise).

Not all of these typefaces are of equal beauty. Here are some examples
of running text, single sentences first:

{{< figure src="/ox-hugo/02-sentencetests.png" >}}

And full paragraphs (for a subset of the typefaces; only including
those that manage at least a majority of the characters and diacritic
combinations):[^fn:2]

{{< figure src="/ox-hugo/03-serifparas.png" >}}

{{< figure src="/ox-hugo/04-sansparas.png" >}}

{{< figure src="/ox-hugo/05-monoparas.png" >}}

Only a few typefaces handle the full range of characters and diacritic
combinations. For the serif faces:

-   CMU Serif
-   Noto Serif
-   Junicode

For the sans faces:

-   CMU Sans Serif
-   Noto Sans
-   Fira Sans (FiraGO)

For the monospaced faces:

-   CMU Typewriter
-   Noto Sans Mono
-   Iosevka

Here is a comparison only of these "winners" (and Noto Sans Mono
perhaps is not a winner, lacking an italic face):

{{< figure src="/ox-hugo/06-winners.png" >}}

So, if you want to use a font family with good performance across the
full range of fonts, Computer Modern Unicode (CMU) or Noto seem like
the best bets, and the former is much more aesthetically-pleasing than
the latter (in my opinion), which also lacks an italic font for its
mono version. Junicode is also a beautiful and very functional serif
face, which I often use.

The Latin Modern faces turn out, despite the received wisdom, to be
inferior in terms of diacritic coverage, and switching from Latin
Modern to Computer Modern Unicode (CMU) has significantly reduced the
amount of frustration I have in trying to typeset my papers.


## Editor typefaces {#editor-typefaces}

The editor font is the one **I** spend most of my time staring at, so I
also want something good here too.

Despite DejaVu Sans Mono apparently not properly rendering all
character/diacritic combinations in LaTeX, it handles all of these
perfectly well when used as the default font in Emacs:

{{< figure src="/ox-hugo/dejavusansmono-emacs.png" >}}

[DejaVu Sans Mono was for many years my preferred font](https://babbagefiles.blogspot.com/2014/12/in-praise-of-dejavu-sans-mono.html) for Emacs (and I
assume DejaVu Sans Mono-derived fonts like [Menlo](https://en.wikipedia.org/wiki/Menlo%5F(typeface)) or [Hack](https://sourcefoundry.org/hack/) will also
behave well).

Noto Sans Mono also works fine, but it's not my favourite:
![](/ox-hugo/notosansmono-emacs.png)

CMU Typewriter, despite rendering well in LaTeX, has problems with
certain combination when used as the Emacs font, and anyway doesn't
look as good as the other choices as an editor font for whatever
reason:[^fn:3]
![](/ox-hugo/cmutypewriter-emacs.png)

These days, I prefer to use a customised version of Iosevka, which
also handles all of the characters/diacritic combinations perfectly:

{{< figure src="/ox-hugo/iosevkaoakvise-emacs.png" >}}

I plan to write more extensively about Iosevka in another post.


## Summary: Best Bets {#summary-best-bets}

For typesetting papers requiring a full-range of unicode roman
characters and diacritics (for non-roman, the Noto fonts are great),
the **Computer Modern Unicode (CMU)** faces[^fn:4]
are the best bets, along with **Junicode**.

For editor work requiring a full-range of unicode roman
characters and diacritics, use a **DejaVu Sans Mono** font or derivative,
or one of the **Iosevka** variants.

[^fn:1]: [Source file](https://gitlab.com/emacsomancer/neobabbage_files/-/raw/master/assets/files/cmu_vs_latinmodern_tests.tex).
[^fn:2]: I'm not sure why Latin Modern Typewriter ends up with a ragged right margin.
[^fn:3]: Ubuntu Mono, when I tried setting it as the default font in Emacs and opening the `.tex` file from which the pdf screenshots shown here were taken, caused Emacs to crash!
[^fn:4]: E.g., CMU Serif, CMU Sans Serif, CMU Typewriter; there are a number of others, including also [CMU Concrete Roman](https://en.wikipedia.org/wiki/Concrete%5FRoman), used below for the non-title text (the title text is set in Latin Modern Dunhill): ![](/ox-hugo/dunhill-computermodernconcrete-faces-lambdacalculusslide.png)

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
