+++
title = "Semi-automated installation of Void Linux on pure ZFS with full LUKS disk encryption"
author = ["Benjamin Slade"]
date = 2019-06-17T15:53:01-06:00
tags = ["encryption", "luks"]
categories = ["zfs", "linux", "automation"]
draft = false
creator = "Emacs 26.2 (Org mode 9.2.4 + ox-hugo)"
+++

After spending too much time repeatedly trying to set up a Void Linux
installation using 'pure ZFS' and full-disk LUKS encryption, I ended
up writing up a set of BASH scripts to automate (and, effectively,
document) the installation process, and prevent me from forgetting
steps along the way. There's quite a number of potential stumbling
block along the way (such as `grub-probe` not knowing how to properly
find the root drive) which I figured out ways around, as well as
discovering that Void's `zfs-0.8.0` package was
[missing a
`python3` dependency which caused ZFS DKMS builds to fail](https://github.com/void-linux/void-packages/issues/12465).

{{<figure src="https://gitlab.com/emacsomancer/full-zfs-and-full-luks-encryption-on-void-linux/raw/master/misc/void-on-zfs.png" alt="Void Linux on ZFS" width="300">}}

The scripts are more or less automated if you're installing in a
particular fashion. They'll ask for user input along the way for
configuring/customising certain things.  What isn't covered is a setup
with multiple vdevs or UEFI or musl, but if you want these things
you'll probably be able to patch the scripts accordingly and perhaps
these options could be accommodated in a future version.

The scripts live here:
<https://gitlab.com/emacsomancer/full-zfs-and-full-luks-encryption-on-void-linux>
, where you'll find additional instructions and information.

(I do recommend using the [Ubuntu Live ISO](https://ubuntu.com/download/desktop) as your installer 'host' for
ease and reduction of the installation time: the 'host' for the
installation doesn't really matter: basically it's just being used to
run the initial `cryptsetup` for the LUKS partition and initial ZFS
pool creation and the host for the Void chroot. The Ubuntu Live CD has
ZFS baked in, so you don't have to wait twice(!) for DKMS to build ZFS
modules.)

Using ZFS for the entire system, from `/` to `/home` to `/boot` also
has the advantage of not requiring you to decide how much space to
allocation ahead of time. With `/boot` on a separate partition, I've
sometimes encountered issues of running out of space on `/boot`
because of maintaining multiple kernels, or else having to massively
overshoot in terms of how much space to give to `/boot`. A full ZFS
install avoids this issue, **and** allows for easy snapshots of the
`/boot` directory.

ZFS is a great file-system if you care about your data. ZFS is most
impressive file-system, and it has a number of other wonderful
features aside from data-integrity, and once you're used to it, you'll
want it everywhere.  For instance, the 'default' lz4 compression is
effectively 'free', in terms of CPU usage (minor CPU hit for dealing
with compression is offset by the need to process smaller chunks of
data), and can be significant: on my root dataset
(`dozer/ROOT/system`) I'm currently getting 1.79x compression: so 22.7G
of logical data is written in 13.6G, and even my dataset full of PDFs
has a more modest 1.03x compression ratio, but this means I save over
3G.

ZFS 0.8.0 also brings with it native encryption. I've not chosen to
use this at the moment, as LUKS makes a full-disk setup easier at
this point, but native encryption could be used in conjunction with
LUKS encryption (potentially useful if, say, you want to backup up
particular ZFS datasets to a remote and not entirely trusted
machine:– natively encrypted ZFS snapshots can be sent without
decrypting the data).

All of these various features, such as compression and encryption can
be enabled per dataset, which allows for great flexibility. After
suffering bit rot, which filtered through and rendered pointless my
carefully maintained versioned backups, I really don't like trusting
my data to any other file-system.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
