+++
title = "Browsing the Web with Common Lisp"
author = ["Benjamin Slade"]
date = 2018-10-20T13:22:00-06:00
tags = ["web"]
categories = ["lisp", "commonlisp", "nextbrowser", "browsers"]
draft = false
creator = "Emacs 28.1 (Org mode 9.5.2 + ox-hugo)"
+++

I was a long-time user of [Conkeror](http://conkeror.org/), a highly-extensible browser with
an Emacs ethos. It [still exists](https://www.freelists.org/archive/conkeror/09-2018), but since the changes in the Firefox
back-end away from XULRunner, which Conkeror uses, running Conkeror
became increasingly difficult to use, so I've largely switched to
just using plain Firefox.

However, [John Mercouris](http://john.mercouris.online/) has been developing [Next Browser](http://next.atlas.engineer/) (originally
styled [nEXT Browser](https://dm.reddit.com/r/programming/comments/7fw57u/next_browser_a_next_generation_extensible_lisp/)), a browser with a Common Lisp front-end, allowing
for customisability and extensibility along Conkeror/Emacs lines:

{{< figure src="https://web.archive.org/web/20190226201246/https://raw.githubusercontent.com/atlas-engineer/next/master/assets/gifs/fast_navigation.gif" >}}

The back-ends are – if I understand correctly – planned to be Blink
for the QT port and WebkitGTK+ for the GTK port, with the Mac port of
Webkit for the Mac version. But the front-end, the user-facing side,
is Common Lisp.

John is currently running an [Indiegogo campaign to properly port](https://www.indiegogo.com/projects/next-browser-nix-support#/) it to
Linux and other non-Mac Unix variants (it apparently runs well already
on the Mac, John's main platform it seems [there's no accounting for
taste ;) ]). The raised money would be used in part to pay a
professional C/C++ developer for their time.

[Ambrevar](https://ambrevar.xyz/) is [currently working on packaging](https://github.com/atlas-engineer/next/issues/92) Next Browser for [Guix](https://www.gnu.org/software/guix/),
which is exciting and promises to add to the amount of Lisp front-end
software we'll be able to use. Currently I'm running Emacs (elisp) for
the majority of my non-browser productivity (writing papers &amp; creating
class slides using [AUCTeX](https://www.gnu.org/software/auctex/); reading composing email with [mu4e](https://www.djcbsoftware.nl/code/mu/mu4e.html);
note-taking and scheduling with [Org mode](https://orgmode.org/); &amp;c. &amp;c.) and, at least on
one machine, [StumpWM](https://stumpwm.github.io/) (Common Lisp window manager) for my 'desktop
environment'; and [GNU GuixSD](https://www.gnu.org/software/guix/) with a Guile-based package manager,
Guile-based cron ([mcron](https://www.gnu.org/software/mcron/manual/html_node/index.html#Top)), and Guile-based init/daemon-manager
([Shepherd](https://www.gnu.org/software/shepherd/)). A functional, configurable, Lisp-based browser would be a
most welcome addition. As excellent as Firefox is, especially its
backend, I do really miss the halcyon days of Conkeror, and Next
Browser could represent a return to those heady days of configurable
browsing Emacs-style.

So, if this sort of thing appeals to you (i.e. if you like Lisp,
Emacs, and/or highly-extendable browsers), you might want to support
the Linux/Unix-port of Next Browser:
<https://www.indiegogo.com/projects/next-browser-nix-support>

There's only about a week left in the campaign.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"