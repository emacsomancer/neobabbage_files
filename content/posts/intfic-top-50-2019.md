+++
title = "Top 50 IF list 2019"
author = ["Benjamin Slade"]
date = 2019-07-30T11:32:00-06:00
tags = ["zork"]
categories = ["intfic", "emacs"]
draft = false
creator = "Emacs 26.2 (Org mode 9.2.4 + ox-hugo)"
+++

Emily Short [wrote a blog post a fortnight ago or so](https://emshort.blog/2019/07/14/a-top-20-list-of-if/) discussing her
nominees for Victor Gijsbers' [Top 50 Interactive Fiction Games of All
Time list, 2019 edition](https://intfiction.org/t/participate-in-the-2019-interactive-fiction-top-50/). The contest closes on the 31st of July 2019
(i.e. in 2 days, as of the day I write this), and I was thinking about
what games would be on my list. This has also resulted, perhaps more
importantly, with me having a list of games I still need to play.

Both lists below have the games hyperlinked to their [Interactive
Fiction Database](https://ifdb.tads.org/) page, which generally includes reviews and game files
to download (or links/suggestions of where to get the game).

First, my personal list of favourites/best text adventures/interactive
fiction that I've completed or at least played most (er, at least
half?) of, ordered roughly in the order I encountered them:

| Game                                                                                       | Year | Author                         | Publisher        |
|--------------------------------------------------------------------------------------------|------|--------------------------------|------------------|
| [Zork I](https://ifdb.tads.org/viewgame?id=0dbnusxunq7fw5ro)                               | 1980 | Marc Blank & Dave Lebling      | Infocom          |
| [Zork II](https://ifdb.tads.org/viewgame?id=yzzm4puxyjakk8c4)                              | 1981 | Dave Lebling & Marc Blank      | Infocom          |
| [Enchanter](https://ifdb.tads.org/viewgame?id=vu4xhul3abknifcr)                            | 1983 | Marc Blank & Dave Lebling      | Infocom          |
| [Planetfall](https://ifdb.tads.org/viewgame?id=xe6kb3cuqwie2q38)                           | 1983 | Steve Meretzky                 | Infocom          |
| [The Hitchhiker's Guide to the Galaxy](https://ifdb.tads.org/viewgame?id=ouv80gvsl32xlion) | 1984 | Douglas Adams & Steve Meretzky | Infocom          |
| [Wishbringer](https://ifdb.tads.org/viewgame?id=z02joykzh66wfhcl)                          | 1985 | Brian Moriarty                 | Infocom          |
| [The Pawn](https://ifdb.tads.org/viewgame?id=a3ege93gjofe5q0m)                             | 1985 | Rob Steggles et al.            | Magnetic Scrolls |
| [Spellbreaker](https://ifdb.tads.org/viewgame?id=wqsmrahzozosu3r)                          | 1985 | Dave Lebling                   | Infocom          |
| [The Guild of Thieves](https://ifdb.tads.org/viewgame?id=bu2v2j5sqpf4usex)                 | 1987 | Rob Steggles                   | Magnetic Scrolls |
| [Knight Orc](https://ifdb.tads.org/viewgame?id=laiyfhqfopw0x9r6)                           | 1987 | Pete Austin                    | Level 9          |
| [Dunnet](https://ifdb.tads.org/viewgame?id=ig3zbeoqfv4v1xl8)                               | 1982 | Ron Schnell                    |                  |
| [Curses!](https://ifdb.tads.org/viewgame?id=plvzam05bmz3enh8)                              | 1993 | Graham Nelson                  |                  |
| [Spider & Web](https://ifdb.tads.org/viewgame?id=2xyccw3pe0uovfad)                         | 1998 | Andrew Plotkin                 |                  |
| [Counterfeit Monkey](https://ifdb.tads.org/viewgame?id=aearuuxv83plclpl)                   | 2012 | Emily Short                    |                  |
| [Coloratura](https://ifdb.tads.org/viewgame?id=g0fl99ovcrq2sqzk)                           | 2013 | Lynnea Glasser                 |                  |
| [Mentula Mancanus: Apocolocyntosis](https://ifdb.tads.org/viewgame?id=etul31tqgl3n22nl)    | 2011 | Adam Thornton                  |                  |

Some of these may be being boosted by nostalgia, but I think they all
pretty much hold up and are interesting/innovative in some fashion.

Looking over the author names, I remember when playing Infocom games
back in the 80s suspecting that "Marc Blank" was a
pseudonym....i.e. Last Name: [blank].

[Dunnet](https://en.wikipedia.org/wiki/Dunnet_(video_game)) is an interesting entry also in that it was originally written
in Maclisp for the DECSYSTEM-20, then ported to Emacs Lisp in 1992,
and it is the Emacs version (`M-x dunnet`) that I'm familiar with.

{{<figure src="/ox-hugo/dunnet-emacs.png">}}

A list of games I still need to play (never played before) or finish
(haven't made significant progress, though I spent a pretty long time
with the Silicon Dreams games, Anchorhead, and Varicella, but don't
think I've made much progress (or I've forgotten what progress I
made)), roughly in order of precedence (i.e. how soon I plan to play):

| Game                                                                                        | Year | Author                         | Publisher |
|---------------------------------------------------------------------------------------------|------|--------------------------------|-----------|
| [Anchorhead](https://ifdb.tads.org/viewgame?id=op0uw1gn1tjqmjt7)                            | 1998 | Michael Gentry                 |           |
| [Cragne Manor](https://ifdb.tads.org/viewgame?id=4x7nltu8p851tn4x)                          | 2018 | [numerous]                     |           |
| [Galatea](https://ifdb.tads.org/viewgame?id=urxrv27t7qtu52lb)                               | 2000 | Emily Short                    |           |
| [Savoir-Faire](https://ifdb.tads.org/viewgame?id=p0cizeb3kiwzlm2p)                          | 2002 | Emily Short                    |           |
| [Hadean Lands](https://ifdb.tads.org/viewgame?id=u58d0mlbfwcorfi)                           | 2014 | Andrew Plotkin                 |           |
| [Varicella](https://ifdb.tads.org/viewgame?id=ywwlr3tpxnktjasd)                             | 1999 | Adam Cadre                     |           |
| [Endless, Nameless](https://ifdb.tads.org/viewgame?id=7vtm1rq16hh3xch)                      | 2012 | Adam Cadre                     |           |
| [Bronze](https://ifdb.tads.org/viewgame?id=9p8kh3im2j9h2881)                                | 2006 | Emily Short                    |           |
| [Balances](https://ifdb.tads.org/viewgame?id=x6ne0bbd2oqm6h3a)                              | 1994 | Graham Nelson                  |           |
| [Jigsaw](https://ifdb.tads.org/viewgame?id=28uhmejlntcbccqm)                                | 1995 | Graham Nelson                  |           |
| [Blue Lacuna](https://ifdb.tads.org/viewgame?id=ez2mcyx4zi98qlkh)                           | 2008 | Aaron Reed                     |           |
| [A Beauty Cold and Austere](https://ifdb.tads.org/viewgame?id=y9y7jozi0l76bb82)             | 2017 | Mike Spivey                    |           |
| [Lime Ergot](https://ifdb.tads.org/viewgame?id=b8mb4fcwmf1hrxl)                             | 2014 | Caleb Wilson (as Rust Blight)  |           |
| [Suveh Nux](https://ifdb.tads.org/viewgame?id=xkai23ry99qdxce3)                             | 2007 | David Fisher                   |           |
| [Delusions](https://ifdb.tads.org/viewgame?id=m85rnpq6x77jyzc3)                             | 1996 | C. E. Forman                   |           |
| [Slouching Towards Bedlam](https://ifdb.tads.org/viewgame?id=032krqe6bjn5au78)              | 2003 | Star Foster & Daniel Ravipinto |           |
| [A Mind Forever Voyaging](https://ifdb.tads.org/viewgame?id=4h62dvooeg9ajtfa)               | 1985 | Steve Meretzky                 | Infocom   |
| [Trinity](https://ifdb.tads.org/viewgame?id=j18kjz80hxjtyayw)                               | 1986 | Brian Moriarty                 | Infocom   |
| [Snowball](https://ifdb.tads.org/viewgame?id=6lgu6t1f65qrdf7o) [Silicon Dreams]             | 1983 | Mike Austin et al.             | Level 9   |
| [Return to Eden](https://ifdb.tads.org/viewgame?id=zr48drl7ctjw0v9a) [Silicon Dreams]       | 1984 | Nick Austin et al.             | Level 9   |
| [The Worm in Paradise](https://ifdb.tads.org/viewgame?id=ohwkcbpfdtjrzrz6) [Silicon Dreams] | 1985 | Mike Austin et al.             | Level 9   |
| [The Shadow in the Cathedral](https://ifdb.tads.org/viewgame?id=g79qfkq3m3dtffq4)           | 2009 | Ian Finley & John Ingold       |           |
| [Make It Good](https://ifdb.tads.org/viewgame?id=jdrbw1htq4ah8q57)                          | 2009 | Jon Ingold                     |           |
| [1893: A World's Fair Mystery](https://ifdb.tads.org/viewgame?id=00e0t7swrris5pg6)          | 2002 | Peter Nepstad                  |           |
| [Red Moon](https://ifdb.tads.org/viewgame?id=yrdxe16idkn87bo8)                              | 1985 | David Williamson et al.        | Level 9   |
| [Shade](https://ifdb.tads.org/viewgame?id=hsfc7fnl40k4a30q)                                 | 2000 | Andrew Plotkin                 |           |
| [Stationfall](https://ifdb.tads.org/viewgame?id=9nlbhqnlyb169uge)                           | 1987 | Steve Meretzky                 | Infocom   |
| [Rameses](https://ifdb.tads.org/viewgame?id=0stz0hr7a98bp9mp)                               | 2000 | Stephen Bond                   |           |
| [The Moonlit Tower](https://ifdb.tads.org/viewgame?id=10387w68qlwehbyq)                     | 2002 | Yoon Ha Lee                    |           |
| [Bureaucracy](https://ifdb.tads.org/viewgame?id=zjyxds3s57pgis3x)                           | 1987 | Douglas Adams                  | Infocom   |

All of the games listed here, in either list, are playable in either
[Malyon](https://melpa.org/#/malyon) or [Gargoyle](http://ccxvii.net/gargoyle/) (with the exception, I think, of Hadean Lands), an admittedly
rather arbitrary criterion.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
