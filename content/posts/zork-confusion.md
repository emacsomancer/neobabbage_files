+++
title = "Confusion: PDP-10 Zork"
author = ["Benjamin Slade"]
date = 2018-09-23T23:52:00-06:00
tags = ["zork"]
categories = ["lisp", "mdl", "intfict", "guix"]
draft = false
creator = "Emacs 26.2 (Org mode 9.2.4 + ox-hugo)"
+++

I grew up playing [Infocom](https://en.wikipedia.org/wiki/Infocom), [Magnetic Scrolls](https://en.wikipedia.org/wiki/Magnetic_Scrolls), and [Level 9](https://en.wikipedia.org/wiki/Level_9_Computing) text
adventures, with the [Zork trilogy](https://en.wikipedia.org/wiki/Zork), the [Enchanter trilogy](https://en.wikipedia.org/wiki/Enchanter_(video_game)), [Planetfall](https://en.wikipedia.org/wiki/Planetfall),
[Wishbringer](https://en.wikipedia.org/wiki/Wishbringer), [The Guild of Thieves](https://en.wikipedia.org/wiki/The_Guild_of_Thieves), [The Pawn](https://en.wikipedia.org/wiki/The_Pawn), [Knight Orc](https://en.wikipedia.org/wiki/Knight_Orc), and [Silicon
Dreams](https://en.wikipedia.org/wiki/Silicon_Dreams) being particularly prominent in my memory (somewhat
re-activated through recent listening to the [Eaten by a Grue
podcast](http://monsterfeet.com/grue/)). I would have played all of these on an Atari 8bit or ST
computer, and didn't have any access to anything like a mainframe, and
so never actually played the [**original** Zork](https://www.filfre.net/2012/01/zork-on-the-pdp-10/), which was written in the
[Lisp-derived MDL language](https://en.wikipedia.org/wiki/MDL_(programming_language)) (which formed the basis for the MDL-subset
Infocom-specific [ZIL language](https://en.wikipedia.org/wiki/Z-machine) used for their subsequent offerings) for
the [DEC PDP-10](https://en.wikipedia.org/wiki/PDP-10).

Fortunately, some years ago, Matthew Russotto created [Confusion](http://www.russotto.net/git/mrussotto/confusion/src/master/src/README), "a
MDL interpreter which works just well enough to play the original Zork
all the way through" and runs on Linux/Unix and presumably
macOS. I'd tried to install this from Arch's AUR a couple of years
ago, without any luck. Inspecting the [package build](https://aur.archlinux.org/cgit/aur.git/tree/PKGBUILD?h=mdli), the AUR Confusion
tries to get around errors by using an ancient version of gcc. I found
that a few fairly trivial fixes were enough to get it to compile with
a recent version of gcc, and so was finally able to try the original
Zork:

{{<figure src="/ox-hugo/mdl-zork-white-house.jpg">}}

The most fun I always had in text adventures was exploring, and I've
always found Infocom's various "time-limit" elements (batteries
running out, health deteriorating, &c.) somewhat irritating. I
remembered that there's a torch in Zork I, and that is also true in
the original Zork, and I managed to obtain it early on so as to save
my (trusty brass) lamp's batteries. I then cheated somewhat wildly
(though, to be fair, a lot of the puzzles I had figured out once upon
a time in Zork I or Zork II; and the bank puzzle and some of the
others I wouldn't have patience for anymore).

The original code has some bugs in it which made things more
interesting.

```ascii
Narrow Ledge
You are on a narrow ledge overlooking the inside of an old dormant
volcano.  This ledge appears to be about in the middle between the
floor below and the rim above. There is an exit here to the south.
There is a very large and extremely heavy wicker basket with a cloth
bag here. Inside the basket is a metal receptacle of some kind.
Attached to the basket on the outside is a piece of wire.
The basket contains:
 A cloth bag
 A braided wire
 A receptacle
 The receptacle contains:
  A newspaper
 A blue label
There is a small hook attached to the rock here.

> untie braided wire from hook

 *ERROR*
 "FIRST-ARG-WRONG-TYPE"
 "First arg to NTH must be structured"
LISTENING-AT-LEVEL 2 PROCESS 1
Atom REP has neither LVAL nor GVAL
```

I had a similar issue involving the result of a disagreement with a
suspicious-looking individual holding a bag. It turns out (thanks to
Matthew Russotto for the following information) that this has to do
with issues in saving and restoring files, and (failure of) either
properly recording or decoding certain values. The balloon issue has
to do with a record about the object burning in the receptacle. It is
saved as `BINF!-FLAG`, which should be a boolean-type flag, but at
some point it became an object (recording what is burning) and
apparently isn't decoded properly on a restore. Saving-and-restoring
during a battle with the suspicious-looking individual produces a
similar error to the balloon-burnable error, due to the
`THIEF-ENGROSSED!-FLAG` (which apparently really is a flag) not
being saved properly. The upshot (for a player) is that you shouldn't
save during either of these bits of the game.

With these additional lurking grues out of the way, I was able to make
it to the end of the game:

{{<figure src="/ox-hugo/zork-end.png">}}

Speaking of saving, MDL Zork only has a single restore file. It is
stored in the `MTRZORK` directory and is named `ZORK.SAVE`. Of course
you can create additional saves by copying out this save file to
different names.

If you too want to play the original Zork using the original PDP-10 MDL
source code, you should be able to build the MDL interpreter necessary
from the release at Matthew Russotto's site (where my minor patches
should be incorporated):
<http://www.russotto.net/git/mrussotto/confusion/releases> or
from the git repo with my patches at
<https://gitlab.com/emacsomancer/confusion-mdl>.

Or, the easiest of all, install it via Guix ([which can installed on
top of your current distro as a standalone package manager](https://www.gnu.org/software/guix/manual/en/html_node/Binary-Installation.html#Binary-Installation)), as I have
created a Guix package which was accepted upstream (the package name
in Guix is `confusion-mdl`).

Since I played through this right before designing the propositional
logic homework for my Semantics class, my students are currently
"enjoying" doing Zork-flavoured propositional logic translations,
e.g.:

1.  The thief only steals your things when you're in the cellar.
2.  Your sword glows blue whenever the thief or the troll is near and
    never any other time.
3.  Whether or not you are lost in a maze of twisty little passages
    (all alike), the grues are lurking in the darkness.
4.  The grues will eat you unless you bring a light source.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
