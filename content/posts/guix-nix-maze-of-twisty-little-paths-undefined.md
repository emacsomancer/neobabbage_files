+++
title = "Guix, Nix: You are in a maze of twisty little $PATHs, some undefined"
author = ["Benjamin Slade"]
date = 2019-07-24T00:48:00-06:00
tags = ["nix", "gargoyle"]
categories = ["lisp", "guix", "linux", "stumpwm", "intfic", "fonts", "inform"]
draft = false
creator = "Emacs 26.2 (Org mode 9.2.4 + ox-hugo)"
+++

Some notes on interactive fiction/text adventure games and PATHs in
Guix, and StumpWM.


## Maze no. 1 {#maze-no-dot-1}

There may (likely is) some way of programmatically setting the X
Windows PATH variable in Guix System (née GuixSD) via the base
configuration (e.g. `config.scm`), but I haven't been able to uncover
anything that works. This is relevant for being able to use locally
installed static binaries or local shell scripts via the window
manager.

As a window-manager-specific workaround, in StumpWM, one can
programmatically set PATH variables via `(setf (getenv
"VARIABLE_NAME") "variable-value")`. Thus, if you store local static
binaries and shell scripts in `~/bin`, the following (which you could
include in StumpWM's `init.lisp`) will add that to your PATH variable:

```lisp
(setf (getenv "PATH") (concat "/home/YOURUSERNAME/bin:" (getenv "PATH")))
```

I use this with a static Haskell binary [greenclip](https://github.com/erebe/greenclip), which adds clipboard
functionality to [rofi](https://github.com/davatorium/rofi/), and with shell scripts that give "pretty names"
to Flatpak run commands.

For example, the literate/natural-language-based programming
interactive fiction design language [Inform7](http://inform7.com/) (which is due to be
[open-sourced sometime this year](http://inform7.com/talks/2019/06/14/narrascope.html)) is now conveniently [available as a
Flatpak](https://flathub.org/apps/details/com.inform7.IDE). But the run command after installing is `flatpak run
com.inform7.IDE`, which is non-ideal. So I made a simple shell script
named `inform7` placed in `~/bin`:

```shell
#!/bin/sh
flatpak run com.inform7.IDE
```

{{<figure src="/ox-hugo/inform7-guix.png">}}


## Maze no. 2 {#maze-no-dot-2}

[Nix](https://nixos.org/) can be [installed as a standalone package manage on top of other
distros](https://nixos.org/releases/nix/nix-1.9/manual/#ch-installing-binary), including Guix System, which is useful for be able to obtain
software currently lacking in Guix System (including, ironically,
[Hugo](https://en.wikipedia.org/wiki/Hugo_(software)), used by this blog, which is present in Nix). Packages available
in Nix but not in Guix include [Gargoyle](http://ccxvii.net/gargoyle/), a very nice interactive
fiction front-end client that supports a number of different backends,
including Frotz and Glulxe. One of the benefits of Gargoyle is that it
"cares about typography". However, Nix applications by default seem to
have trouble finding/seeing fonts, including system fonts, local
fonts, and even fonts installed via Nix.

This can be fixed by (1) setting the `FONTCONFIG_PATH` and
`FONTCONFIG_FILE`, e.g. in StumpWM this can be done with:

```lisp
(setf (getenv "FONTCONFIG_PATH") "/home/YOURUSERNAME/.config/fontconfig/")
(setf (getenv "FONTCONFIG_FILE") "fonts.conf")
```

And (2) forcing Nix to look in the right places by manual
specification in `~/.config/fontconfig/fonts.conf`, adding right
before the final `</fontconfig>` (as appropriate):

```nil
<cachedir prefix="xdg">fontconfig</cachedir>
<dir>/home/YOURUSERNAME/.local/share/fonts/</dir>
<dir>/home/YOURUSERNAME/.nix-profile/share/fonts/</dir>
<dir>/home/YOURUSERNAME/.guix-profile/share/fonts/</dir>
<dir>/usr/share/fonts</dir>
```

And regenerating the font cache (via `fc-cache -fv`) [possibly you may
need to install Nix's `fontconfig` package].

{{<figure src="/ox-hugo/counterfeit-gargoyle-screenshot.png">}}

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
