+++
title = "Twiddler config for Emacs"
author = ["Benjamin Slade"]
date = 2020-05-30T22:00:00-06:00
categories = ["emacs", "twiddler"]
draft = false
creator = "Emacs 28.0.50 (Org mode 9.3.6 + ox-hugo)"
+++

The [Twiddler](https://twiddler.tekgear.com/) [[here's archive.org's link](https://web.archive.org/web/20191212214708/https://twiddler.tekgear.com/), as the site seems to be down
as I write this], a one-handed chording keyboard, has a longish history of
being associated with Emacs. Here's 1990s [Alan Alda](https://en.wikipedia.org/wiki/Alan%5FAlda) interviewing
[Thad Starner](https://en.wikipedia.org/wiki/Thad%5FStarner), who's using a wearable-computing device foreshadowing
Google Glass, using a Twiddler mk 1 to interact with Emacs (using the
[Remembrance Agent](http://alumni.media.mit.edu/~rhodes/Papers/remembrance.html)):

<div class="org-youtube"><iframe src="https://www.youtube-nocookie.com/embed/X7DM1mT8r7c?start=2151" allowfullscreen title="YouTube Video"></iframe></div>

I've long been intrigued by this one-hand, non-tethered input method
and finally got a Twiddler 3. I was unfortunately stymied on progress
early on due to a loose battery (requiring cracking the entire device
open), but have got it back working and am slowly trying to integrate
it into my workflow.

One place where it would be particularly useful is with a mobile
device (e.g. smart phone), especially as one can run full [Emacs in
Termux on Android](https://endlessparentheses.com/running-emacs-on-android.html).

The Twiddler is a configurable device, and obviously using it with
Emacs calls for some Emacs-specific configuration. Here is my current
working configuration:

[M-x tab-space: Emacs-centric layout for the Twiddler 3](https://gitlab.com/emacsomancer/m-x-tabspace)

With important Emacs combinations like `C-x`, `M-x`, `C-g` mapped to
chords (along with a chord prefix, `s-F` for my personal [StumpWM](https://stumpwm.github.io/)
config), and some chords set up for comfortable navigating with [God
Mode](https://github.com/emacsorphanage/god-mode) and [avy](https://github.com/abo-abo/avy). And lots of space for expansion.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
