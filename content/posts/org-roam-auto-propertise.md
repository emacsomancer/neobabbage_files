+++
title = "Automatically adding information to Org-roam file properties"
author = ["Benjamin Slade"]
date = 2021-08-10T21:12:00-06:00
tags = ["orgroam"]
categories = ["emacs", "org"]
draft = false
creator = "Emacs 28.0.50 (Org mode 9.4.6 + ox-hugo)"
+++

This expands on a feature I included in the setup for using [Org-roam](https://www.orgroam.com/)
on Android/LineageOS in the [last post](../org-roam-on-android), specifically automatically
adding properties to newly created Org-roam files.

Since Org-roam v2 creates a top properties drawer (with an `:ID:` tag) anyway, it is nice to stick other information there as well. Specifically, information that could be useful in some situation, but which usually we don't want to see, like `:AUTHOR:` (it's probably you, and you know who you are), `:CREATION_TIME:` (and why not use Unix epoch time?), and so on. I have org drawers fold themselves automatically, so the normally-useless information doesn't distract me.

We can do this by leveraging Org-roam's `org-roam-capture-new-node-hook`, and some `org-roam-add-property` function calls, as below.

But, while we're at it, we might also record **where** a note was made from. There are a number of ways we might do this, but an easy one (only requiring `curl` and an active Internet connection) is using [ipinfo.io](https://ipinfo.io/developers/data-types#geolocation-data). `curl ipinfo.io` will give you a bunch of information in JSON format about your internet provider, including latitude and longitude, which will likely be at least somewhere **near** your present location. And `curl ipinfo.io/loc` will return just latitude,longitude.

```elisp
  (defun bms/add-other-auto-props-to-org-roam-properties ()
    ;; if the file already exists, don't do anything, otherwise...
    (unless (file-exists-p (buffer-file-name))
      ;; if there's also a CREATION_TIME property, don't modify it
      (unless (org-find-property "CREATION_TIME")
        ;; otherwise, add a Unix epoch timestamp for CREATION_TIME prop
        ;; (this is what "%s" does - see http://doc.endlessparentheses.com/Fun/format-time-string )
        (org-roam-add-property
         (format-time-string "%s"
                             (nth 5
                                  (file-attributes (buffer-file-name))))
         "CREATION_TIME"))
      ;; similarly for AUTHOR and MAIL properties
      (unless (org-find-property "AUTHOR")
        (org-roam-add-property roam-user "AUTHOR"))
      (unless (org-find-property "MAIL")
        (org-roam-add-property roam-email "MAIL"))
      ;; also add the latitude and longitude
      (unless (org-find-property "LAT_LONG")
        ;; recheck location:
        (bms/get-lat-long-from-ipinfo)
        (org-roam-add-property (concat (number-to-string calendar-latitude) "," (number-to-string calendar-longitude)) "LAT-LONG"))))

  ;; hook to be run whenever an org-roam capture completes
  (add-hook 'org-roam-capture-new-node-hook #'bms/add-other-auto-props-to-org-roam-properties)

;; function to find latitude & longitude
;;                      (requires curl to be installed on system)
(setq calendar-latitude 0)
(setq calendar-longitude 0)
(defun bms/get-lat-long-from-ipinfo ()
  (let*
      ((latlong (substring
                 (shell-command-to-string "curl -s 'ipinfo.io/loc'")
                   0 -1))
       (latlong-list (split-string latlong ",")))
    (setq calendar-latitude (string-to-number (car latlong-list)))
    (setq calendar-longitude (string-to-number (cadr latlong-list)))))
```

You might also calculate/set `calendar-latitude` and `calendar-longitude` in other ways. Including just hard-coding them for stationary machines. On Android, we could in theory make use of the Termux command `termux-location`, which queries the device's GPS. But unfortunately it doesn't always work (if it can't find a good connection to a GPS satellite) and even when it does work it's slow, so it's not something you'd want to call every time you made a note. [GeoClue](https://gitlab.freedesktop.org/geoclue/geoclue/-/wikis/home) would be another possible source.

(If you're using a VPN, you'll want to escape from it somehow to get something closer to your real location. How you do this will vary based on your VPN provider and other factors. (If you're calling from Emacs, and you use something like Mullvad, you may want to revise the `shell-command-to-string` to call up a bash session/script, then exclude that specific bash session/script from the VPN, and _then_ call `curl`, so that the call references your "real" IP. E.g. if you're using Mullvad, then:

```shell
#!/bin/bash
PID=`echo $$`
mullvad split-tunnel pid add "${PID}"
curl ipinfo.io/loc # for lat/long ; `curl ipinfo.io` for full info
```

might give you a start on something.))

Let me know if you think of other properties that could be useful to automatically add to Org-roam file properties.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
