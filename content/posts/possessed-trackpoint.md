+++
title = "Dealing with possessed TrackPoints on later model ThinkPads"
author = ["Benjamin Slade"]
date = 2022-06-18T19:51:00-06:00
categories = ["thinkpad", "linux", "guix"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.3 + ox-hugo)"
+++

On a new-to-me ThinkPad T440p, I've had the worst time with the
TrackPoint.

First, the stock configuration has a horrible touchpad -
which shouldn't matter if you don't use the touchpad, but the
horribleness of it is that the physical buttons that should be on the
top of the touchpad, and are on the touchpads of models preceding and
following the \*\*40 line, are not there. But one [can replace it](https://octoperf.com/blog/2018/11/07/thinkpad-t440p-buyers-guide/#trackpad), and so
I did.

The T440p is nice in that servicing the fan and other internals of the
machine is a relatively easy affair compared to say an X230. Just undo
two screws on the bottom of the laptop and slide off the back panel,
and you have access to memory, drives, the CPU, and so on. And so
swapping in a different CPU was really easy and painless.

On the other hand, changing the touchpad was a very involved
affair. But it [can be done](https://www.youtube.com/watch?v=7wM4Kqdy3_E).

However, still the mouse-cursor experience for the machine continued
to be horrible. Firstly, there was significant "drift" of the
TrackPoint. I.e., even once pressure is released, it keeps moving, for
a long time.

However, this ends up being solvable via

```shell
sudo -s echo  'ACTION=="add",SUBSYSTEM=="input",ATTR{name}=="TPPS/2 IBM TrackPoint",ATTR{device/drift_time}="30"'  > /etc/udev/rules.d/10-trackpoint.rules
```

(If you're trying to do this in Guix, something like:

```scheme
(define %trackpoint-drift-rule
  (udev-rule
    "10-trackpoint.rules"
    (string-append "ACTION==\"add\",SUBSYSTEM==\"input\",ATTR{name}==\"TPPS/2 IBM TrackPoint\",ATTR{device/drift_time}=\"25\""))
```

```scheme
(define %my-desktop-services
   (cons (udev-rules-service 'trackpoint-drift %trackpoint-drift-rule)
         (modify-services %desktop-services
                          ....) ; other modifications here
         ))
```

```scheme
(operating-system
  ....
  (services
    (append
     (list
      (service openssh-service-type)
      (service cups-service-type)
      (service nix-service-type)
      ....)
     %my-desktop-services))
  ....)
```

instead.)

But, unfortunately, this doesn't solve what is actually the most horrible issue:
the mouse cursor sometimes, when in use, just teleports around the edges
of the screen and starts randomly clicking on things.

I finally turned up some discussion of this issue (though not for the
T440p specifically) at: <https://bugzilla.kernel.org/show_bug.cgi?format=multiple&id=209167>
(The discussion suggests that it should somehow be solved in the
kernel, but that is not my experience, even running kernel 5.17.13.)

And found that adding `psmouse.proto=imps` to the kernel arguments and
disabling the touchpad in the stock BIOS solves the "possessed mouse
cursor" issue. For better or worse, it seem to make the TrackPoint be
detected as a generic PS/2 mouse. Which means that `drift_time` can no
longer be set, and I do get a little bit of drift from time to time,
but it's not too bad and certainly is far less maddening than the
"possessed mouse cursor" behaviour.

For Guix, the way to implement this is something along the lines of:

```scheme
(operating-system
 ...
 (kernel-arguments (cons* "modprobe.blacklist=pcspkr,snd_pcsp" "psmouse.proto=imps" "acpi_osi=Linux" %default-kernel-arguments))
 ...)
```

(The other kernel arguments are simply the other ones I use, and are
not directly connected with this issue.)

Depending on the model of ThinkPad and/or the environment, using
`psmouse.proto=bare` instead may work better (see discussion at:
<https://www.reddit.com/r/thinkpad/comments/v7vn0o/thinkpad_t440p_trackpoint_occasionally_going_crazy/icjkk0o/>
).

So, perhaps not an entirely satisfactory solution, this impish
exorcism, but better than alternatives.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"