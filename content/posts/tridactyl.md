+++
title = "Free keybinding with Tridactyl in Firefox, and in-Emacs editing"
author = ["Benjamin Slade"]
date = 2021-10-27T14:42:00-06:00
tags = ["web"]
categories = ["browsers", "emacs"]
draft = false
creator = "Emacs 29.0.50 (Org mode 9.5 + ox-hugo)"
+++

Since the effective demise of the [Conkeror web browser](https://en.wikipedia.org/wiki/Conkeror), I've mainly
been using Firefox (with some experimentation with [Nyxt browser](https://nyxt.atlas.engineer/)). I've
missed the ability to quickly browse with the keyboard and customise
keybindings.

I've played with the [Tridactyl extension](https://github.com/tridactyl/tridactyl) for a few years, but Firefox
limitations in part have kept me from using it more extensively. But I
stumbled across a relatively easy way of "unreserving" reserved
Firefox keys (like `<C-p>`, `<C-f>` etc.) via an [offhand comment at
Lobste.rs](https://lobste.rs/s/9uc1xe/why%5Fshould%5Fi%5Fuse%5Fnyxt%5Fif%5Fi%5Fcan%5Fuse%5Fvimium#c%5Fullicn), which points to [a 2 line bit of code to remove reserved
keybindings](https://github.com/alerque/que-firefox/blob/master/bin/patch-firefox.sh). I repeat it here:

```shell
#!/usr/bin/env sh

sudo perl -i -pne 's/reserved="true"/               /g' /usr/lib/firefox/browser/omni.ja
find ~/.cache/mozilla/firefox -type d -name startupCache | xargs rm -rf
```

This now frees up any key as a potential target for Tridactyl
bindings. It's still not perfect: the ability to remap keys in [StumpWM](https://stumpwm.github.io/)
is still easier, but it gives me a better possibility for
customisation on machines not running StumpWM.

One thing I've added to all machines is a better way of dealing with
Tridactyl's function to edit browser text areas in an external browser
(bound to `C-i` by default). In Tridactyl I run in the command area
`:set editorcmd emacsclient -c %f`. This opens up an Emacs frame
connected to the default daemon. However, I wanted a quick way of
"entering"/"saving" the text and closing the frame. An ad-hoc major
mode seemed like an easy way, allowing for save and close-frame with
`C-c C-c`:

```elisp
;; tridactyl mode
(define-derived-mode tridactyl-mode text-mode "tridactyl"
  "A major mode to edit tridactyl-spawned 'editor' text.")
(define-key tridactyl-mode-map (kbd "C-c C-c")
  '(lambda ()
     "save and exit quickly"
     (interactive)
     (save-buffer)
     (clipboard-kill-ring-save (point-min) (point-max)) ; just in case
     (sleep-for 1)
     (delete-frame)))
(provide 'tridactyl-mode)
```

However, the editorcmd interface seems to have problems running `-e
'(func)'` type calls (maybe there's a way to get it to work, but it
wasn't obvious to me), so I additionally added an `auto-mode-alist`
regexp to auto-detect tridactyl temporary files:

```elisp
(add-to-list 'auto-mode-alist '("/tmp/tmp.*\\.txt$" . tridactyl-mode))
```

And this seems to work well. I still can't quite get in-Firefox text
editing to have Emacs-style bindings, but for anything non-trivial, I
can just edit it in Emacs directly in this way.

(Addendum: though I note that at least a number of text boxes seem to
require some additional input afterwards to "fix" the editorcmd text;
otherwise they revert back to their previous state. A bit
irritating. I've added a save-to-clipboard bit in the function above
as a safety precaution against lost text.)

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
