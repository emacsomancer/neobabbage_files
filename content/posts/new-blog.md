+++
title = "New blog using Emacs, Org mode & Hugo"
author = ["Benjamin Slade"]
date = 2018-06-07T20:26:00-06:00
tags = ["go"]
categories = ["emacs", "org", "hugo"]
draft = false
creator = "Emacs 26.1 (Org mode 9.1.13 + ox-hugo)"
+++

I've been wanting to have an Emacs-powered blog for some
time. Finally, thanks largely to the yeoman's work put in by [Kaushal
Modi](https://scripter.co) on [`ox-hugo`](https://ox-hugo.scripter.co/), an exporter from native Org mode format to
Hugo-ready markdown files, as well his theme/configuration for Hugo,
[`hugo-refined`](https://gitlab.com/kaushalmodi/hugo-theme-refined) (which the theme used here is largely based on), I
finally have an ideal Emacs-centric blogging environment, though I'm
sure I'll continue to tweak things a bit.

[Hugo](https://gohugo.io/) itself is a static website generator, which interprets [markdown](https://en.wikipedia.org/wiki/Markdown)
files. It's written in [Go](https://golang.org/), a language developed at Google, which is
notable for including [Ken Thompson](https://en.wikipedia.org/wiki/Ken_Thompson) and [Rob Pike](https://en.wikipedia.org/wiki/Rob_Pike) among its creators.

This is more or less a continuation of my old blog, of [a similar name](https://babbagefiles.blogspot.com/),
and will largely address Emacs/elisp and other lispy things, and
Linux/UNIX/\*nix and free/open source software and operating systems,
but, like my old blog, probably also occasionally [games](https://babbagefiles.blogspot.com/search/label/minecraft) and [pre-20th
century technology](https://babbagefiles.blogspot.com/2010/10/new-babbage-analytical-engine.html), as well as whatever else catches my interest
(other than natural language things, which I'll keep on my
[professional site](https://slade.jnanam.net/post/)).

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
