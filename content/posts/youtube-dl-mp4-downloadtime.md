+++
title = "youtube-dl bash shell function: prefer mp4, and timestamp with download time"
author = ["Benjamin Slade"]
date = 2019-05-27T22:17:00-05:00
categories = ["shell", "bash", "youtubedl"]
draft = false
creator = "Emacs 30.0.93 (Org mode 9.8-pre + ox-hugo)"
+++

In case it's useful, a quick Bash shell function which takes a single
argument (a web address that is processable by `youtube-dl`) and
returns the best quality mp4 version (in case you need to deal with a
device that doesn't like modern video encodings/containers), with the
**download time** as the file's modification time timestamp (useful if
you have a directory of downloaded videos and want to quickly see the
last N files you downloaded, rather than the files being sorted by
upload time).

Add to your `~/.bashrc` or other shell configuration file as appropriate:

```shell
youtubemp4() {
    youtube-dl -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/mp4' "$1"
    touch "$(youtube-dl -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/mp4' --get-filename $1)"
}
```

Then call in the command-line, e.g. `youtubemp4
https://www.youtube.com/watch?v=Gnnb6sjgk3A`.


[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
