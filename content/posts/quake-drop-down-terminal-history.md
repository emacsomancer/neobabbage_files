+++
title = "Towards a history of Quake-style drop-down terminals"
author = ["Benjamin Slade"]
date = 2022-06-26T20:19:00-06:00
categories = ["terminals"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.3 + ox-hugo)"
+++

Continued work on fooling Emacs into behaving like a drop-down console
(i.e. [Equake](https://babbagefiles.xyz/categories/equake/)), set me to thinking about the development of
Quake-style drop-down terminals.

The frequent label "Quake-style" does seem to suggest at least part of
the origin in the computer game [Quake](https://en.wikipedia.org/wiki/Quake_(video_game)) (1996), or at least that the drop-down console in
Quake was the most prominent/remembered example of this sort of UI.[<span class="org-target" id="org-target--ddt0t"></span>[0](#org-target--ddt0b)]

On Linux/Unix, a number of terminal emulators have been designed with
Quake-style drop-down interaction, and other platforms now seem to
have these as well. As far as I can tell, drop-down terminal emulators
first appeared on Linux, and probably in the early 2000s. (Though I
wonder about this, both the KDE project started in 1996 (same year as
Quake) and the GNOME project shortly after and it almost feels like
someone must have thought about doing something of this sort between
1996-2000.)

The earliest surviving drop-down terminals on Linux seem to be
[Yakuake](https://github.com/KDE/yakuake) (QT/KDE), [Tilda](https://github.com/lanoxx/tilda) (GTK), and [Guake](https://github.com/Guake/guake) (GTK).

Yakuake seems to have been released in the early 2000s, certainly by
2005[<span class="org-target" id="org-target--ddt1t"></span>[1](#org-target--ddt1b)], but perhaps a bit earlier. Tilda was released by 2006[<span class="org-target" id="org-target--ddt2t"></span>[2](#org-target--ddt2b)]. Guake
was explicitly inspired by the original author seeing
Yakuake[<span class="org-target" id="org-target--ddt3t"></span>[3](#org-target--ddt3b)], itself being released in 2007.

But "Yakuake" stands for "yet another Kuake", and indeed the earliest
drop-down terminal I can find on Linux is [Kuake](https://web.archive.org/web/20040219101722/http://www.nemohackers.org/kuake.php), released probably by 2003,
with the last update in 2004[<span class="org-target" id="org-target--ddt4t"></span>[4](#org-target--ddt4b)].

Happy to hear about further history of early drop-down Quake-style
terminals on any platform.

[<span class="org-target" id="org-target--ddt0b"></span>[0](#org-target--ddt0t)] <https://en.wikipedia.org/wiki/Console_(computer_games)>

[<span class="org-target" id="org-target--ddt1b"></span>[1](#org-target--ddt1t)] <https://web.archive.org/web/20051016011941/http://yakuake.uv.ro/>

[<span class="org-target" id="org-target--ddt2b"></span>[2](#org-target--ddt2t)] <https://web.archive.org/web/20061028182927/http://tilda.sourceforge.net/wiki/index.php/Main_Page>

[<span class="org-target" id="org-target--ddt3b"></span>[3](#org-target--ddt3t)] <https://news.ycombinator.com/item?id=22524552>

[<span class="org-target" id="org-target--ddt4b"></span>[4](#org-target--ddt4t)]
<https://web.archive.org/web/20040219101722/http://www.nemohackers.org/kuake.php>

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"