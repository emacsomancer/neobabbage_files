+++
title = "Running pdfpc in StumpWM"
author = ["Benjamin Slade"]
date = 2019-07-29T15:27:00-06:00
tags = ["pdfpc", "pdf", "presentation", "beamer"]
categories = ["lisp", "stumpwm"]
draft = false
creator = "Emacs 26.2 (Org mode 9.2.4 + ox-hugo)"
+++

[`pdfpc`](https://pdfpc.github.io/) is a fantastic application for presenting PDF slides,
including perhaps especially those produced using [LaTeX Beamer](https://ctan.org/pkg/beamer). It
creates two (full-screen) windows, one a presenter viewer which shows
the time elapsed and a preview of the next slide, and one the
presentation view which is what is shown to the audience. It also has
a bunch of other cool features like being able to draw on slides;
highlight areas of slides, &c.

Here is an example, with the presenter's view shown on the left; the
audience's view on the right:

{{<figure src="/ox-hugo/pdfpc-screenshot01.png">}}

In many environments `pdfpc` is pretty smart about getting the
presentation view to the external display, but in StumpWM both end up
getting created in the same frame in the same display.

(General tip: in StumpWM, after connecting/activating an external
display, you may need to run [`refresh-heads`](https://stumpwm.github.io/git/stumpwm-git_9.html#External-Monitors) to get StumpWM to display
things properly.)

For whatever reason, I can't get any of the [Screen-related things](https://stumpwm.github.io/git/stumpwm-git_9.html) in
StumpWM to behave like I have more than one
screen. E.g. `*screen-list*` shows a singleton list even when an
external display is connected and activated:

```lisp
STUMPWM> *screen-list*
(#S<screen #<XLIB:SCREEN :0.0 1366x768x24 TRUE-COLOR>>)
```

A passable solution is to use [`define-frame-preference`](https://stumpwm.github.io/git/stumpwm-git_5.html#Rule-Based-Window-Placement), adding the
following to your `init.lisp` StumpWM configuration:

```lisp
;; pdfpc rule - for 'Default' group
;;  move to frame '1';
;;  non-focussing ('nil) the presentation view;
;;  not only matching (t) the target-group ('Default');
;;  moving the 'pdfpc' 'presentation' window
(define-frame-preference "Default"
  (1 nil t :instance "pdfpc" :role "presentation"))
```

This should properly move the presentation window to the external
display when `pdfpc` is run.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
