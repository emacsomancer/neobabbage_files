+++
title = "Equake(!) Quake-style overlay console in StumpWM"
author = ["Benjamin Slade"]
date = 2019-05-22T22:26:00-06:00
categories = ["lisp", "stumpwm", "equake", "emacs", "eshell"]
draft = false
creator = "Emacs 28.1.50 (Org mode 9.5.3 + ox-hugo)"
+++

I've been alternatively using both KDE Plasma 5 and StumpWM on various
machines and have got a working model for using [the Equake drop-down](https://babbagefiles.xyz/equake-elisp-console/)
in StumpWM.

The StumpWM `#'invoke-equake` command hides (using StumpWM native
`hide-window`, rather than Emacs's `make-frame-invisible` as the
latter creates various issues in finding and fetching the Equake
window) the Equake frame if it's the currently active window; it
searches through all windows in all groups on the current
screen/monitor, and calls `emacsclient -n -e '(equake-invoke)'` to
create an Equake frame if no extant Equake window is found; and if an
Equake window does already exist for the current screen, it is yanked
into the current group, pulled into the current frame, and unhidden (if
necessary).

This seems to work pretty well, though I haven't figured out how to
have StumpWM 'skip' the Equake frame when cycling through Emacs
windows. (And, of course, having a proper partial height floating
Equake window would be ideal.)

**EDIT:** Here's a better, more Quake-like set-up, which actually makes
use of recently added partial-height floating windows in non-floating
groups:

```lisp
(defun calc-equake-width ()
  (let ((screen-width (caddr (with-input-from-stringp (s (run-shell-command "emacsclient -n -e '(equake-find-workarea-of-current-screen (equake-calculate-mouse-location (display-monitor-attributes-list)) (display-monitor-attributes-list))'" t)) (read s))))
        (desired-width-perc (read-from-string (run-shell-command "emacsclient -n -e 'equake-size-width'" t))))
    (truncate (* screen-width desired-width-perc))))

(defun calc-equake-height ()
  (let ((screen-height (cadddr (with-input-from-string (s (run-shell-command "emacsclient -n -e '(equake-find-workarea-of-current-screen (equake-calculate-mouse-location (display-monitor-attributes-list)) (display-monitor-attributes-list))'" t)) (read s))))
        (desired-height-perc (read-from-string (run-shell-command "emacsclient -n -e 'equake-size-height'" t))))
    (truncate (* screen-height desired-height-perc))))

(setq *equake-width* 1368) ; TODO: programmatically get screen dimensions before Emacs starts
(setq *equake-height* 768)

(defcommand invoke-equake () ()
  (let* ((on-top-windows (group-on-top-windows (current-group)))
         (equake-on-top (find-equake-in-group on-top-windows)))
    (if equake-on-top
        (progn (setf (group-on-top-windows (current-group)) (remove equake-on-top on-top-windows))
               (unfloat-window equake-on-top (current-group))
               (hide-window equake-on-top)) ;; then hide Equake window via native Stumpwm method.)
      (let ((found-equake (find-equake-globally (screen-groups (current-screen))))) ; Otherwise, search all groups of current screen for Equake window:
        (if (not found-equake)          ; If Equake cannot be found,
            (progn
              (run-shell-command "emacsclient -n -e '(equake-invoke)'") ; then invoke Equake via emacs function.
              (setq *equake-height* (calc-equake-height)) ; delay calculation of height & width setting until 1st time equake invoked
              (setq *equake-width* (calc-equake-width))) ; (otherwise Emacs may not be fully loaded)
          (progn (raise-window found-equake)
                   (move-window-to-group found-equake (current-group)) ; But if Equake window is found, move it to the current group,
                   (unhide-window found-equake) ; unhide window, in case hidden
                   (float-window found-equake (current-group)) ; float window
                   (float-window-move-resize (find-equake-globally (screen-groups (current-screen))) :width *equake-width* :height *equake-height*) ; set size
                   (focus-window found-equake)
                   (toggle-always-on-top))))))) ; make on top

(defun find-equake-in-group (windows-list)
  "Search through WINDOWS-LIST, i.e. all windows of a group, for an Equake window. Sub-component of '#find-equake-globally."
  (let ((current-searched-window (car windows-list)))
    (if (equal current-searched-window 'nil)
        'nil
        (if (search "*EQUAKE*[" (window-name current-searched-window))
            current-searched-window
            (find-equake-in-group (cdr windows-list))))))

(defun find-equake-globally (group-list)
  "Recursively search through GROUP-LIST, a list of all groups on current screen, for an Equake window."
  (if (equal (car group-list) 'nil)
      'nil
      (let ((equake-window (find-equake-in-group (list-windows (car group-list)))))
        (if equake-window
            equake-window               ; stop if found and return window
            (find-equake-globally (cdr group-list))))))

;; Set the mouse focus policy;  - :click is best for proper Equake functioning
(setf *mouse-focus-policy* :click) ;; also StumpWM default (I think)

;; Keybindings
;; bind to an appropriate key
(define-key *top-map* (kbd "F12") "invoke-equake")
```

This is what it looks like:
![](https://gitlab.com/emacsomancer/equake/raw/master/image/equake-in-stumpwm.gif)

[video
link here](https://gitlab.com/emacsomancer/equake/raw/master/image/equake-in-stumpwm.webm)

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"