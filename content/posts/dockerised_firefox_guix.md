+++
title = "Dockerised Firefox on GuixSD"
author = ["Benjamin Slade"]
date = 2018-09-15T20:18:00-06:00
categories = ["lisp", "guix", "docker", "firefox"]
draft = false
creator = "Emacs 26.1 (Org mode 9.1.14 + ox-hugo)"
+++

So GuixSD doesn't currently package Firefox ([though hopefully that
is changing](https://lists.gnu.org/archive/html/guix-devel/2018-05/msg00021.html)), but only IceCat (which is now EOL). On freenode#guix,
pkill9 suggested that Firefox (and Chromium etc.) could be installed
on Guix via the [Nix](https://nixos.org/nix/download.html) installer (install as per instructions on their
site and then `nix-env -i firefox`) with the following trick, create a
file `~/.local/bin/firefox` with the following content:

```shell
# Wrapper to run the Firefox built and packaged by Nix
MESA_LIB=$(dirname $(realpath /run/current-system/profile/lib/libGL.so)) #To get webgl working
export LD_LIBRARY_PATH="$MESA_LIB${LD_LIBRARY_PATH:+:}$LD_LIBRARY_PATH"
#export FONTCONFIG_PATH="$(guix build fontconfig)/etc/fonts${FONTCONFIG_PATH:+:}$FONTCONFIG_PATH"
export FONTCONFIG_PATH="$(guix build fontconfig)/etc/fonts"
exec -a "$0" "/nix/var/nix/profiles/per-user/$USER/profile/bin/firefox" "$@"
```

And then add `~/.local/bin` to your `$PATH` in `~/.profile`.

Unfortunately I haven't been able to get that to work, though I could
be doing something daft.

I figured out the following (elaborate) alternative workaround for
Firefox (no luck for Chromium):

-   1. Install Docker via Nix. Launch with `sudo dockerd`.
-   2. Create a Dockerfile for Firefox

```Dockerfile
# firefox in a docker container
# the following line will start firefox in the container, thus
# video and sound will be played on the host machine
FROM alpine:edge
MAINTAINER slade@jnanam.net

# add testing repo + install packages + add user and group
RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing/" >> /etc/apk/repositories \
    && apk add --no-cache \
       xz \
       dbus-x11 \
       ttf-dejavu \          # a bunch of fonts; you may not want all of these
       ttf-freefont \
       ttf-ubuntu-font-family \
       font-noto \
       font-noto-extra \
       font-noto-emoji \
       font-noto-oriya \
       font-noto-tamil \
       font-noto-avestan \
       font-noto-gothic \
       font-noto-myanmar \
       font-noto-telugu \
       font-noto-deseret \
       font-noto-bengali \
       font-noto-kannada \
       font-noto-ethiopic \
       font-noto-armenian \
       font-noto-tibetan \
       font-noto-sinhala \
       font-noto-gurmukhi \
       font-noto-malayalam \
       font-noto-gujarati \
       font-noto-devanagari \
       font-noto-thai \
       font-noto-adlam \
       font-noto-nko \
       font-noto-lisu \
       font-noto-carian \
       font-noto-buhid \
       font-noto-osage \
       font-noto-hebrew \
       font-noto-arabic \
       font-noto-chakma \
       font-noto-gothic \
       font-noto-khmer \
       font-noto-cypriot \
       font-noto-kayahli \
       font-noto-mandaic \
       font-noto-olchiki \
       font-noto-thaana \
       font-noto-georgian \
       font-noto-shavian \
       font-noto-cherokee \
       font-noto-oldturkic \
       font-noto-osmanya \
       font-noto-glagolitic \
       font-noto-tifinagh \
       font-noto-adlamunjoined \
       font-noto-nko \
       font-noto-lao \
       arc-theme \
       hunspell \
       hunspell-en \
       firefox \
       libcanberra-gtk2 \
       pulseaudio \
    && rm -fr /var/cache/apk/* \
    && adduser -D -u 1000 -g 1000 user

# add user's work
WORKDIR /home/user

# switch to user
USER user

ENTRYPOINT ["/usr/bin/firefox", "--no-remote"]
```

Save the above out to a file `Dockerfile` in some directory. `cd` to
that directory. Then create a Docker container with `docker build -t
someprefix/firefox .` (with `someprefix` being whatever you like).

-   3. Create in `~/.local/bin/firefox`:

```shell
#!/bin/sh
# Wrapper to run the Chromium built and packaged by Nix
xhost +local:docker@; \
docker run --rm -it -e DISPLAY=$DISPLAY -v /tmp/.X11-unix/:/tmp/.X11-unix \
-v /dev/snd:/dev/snd \
-v /run/user/$USER_UID/pulse:/run/pulse:ro \
-v /home/$USER/.mozilla:/home/user/.mozilla \
-v /home/$USER/.cache/mozilla:/home/user/.cache/mozilla \
-v /tmp/Downloads:/tmp/Downloads \
-v /home/$USER/.gtkrc-2.0:/home/user/.gtkrc-2.0 \
-v /home/$USER/.config/gtk-3.0/:/home/user/.config/gtk-3.0 \
-v /home/$USER/.nix-profile/share/fonts/truetype:/usr/share/fonts/truetype-nix \ # for nix fonts, if you have them here
-v /home/$USER/.nix-profile/share/fonts/ubuntu:/usr/share/fonts/ubuntu-nix \     # ditto
-v /home/$USER/.nix-profile/share/fonts/noto:/usr/share/fonts/noto-nix \         # ditto
-v /home/$USER/.guix-profile/share/fonts/truetype:/user/share/fonts/truetype-guix \ # for guix fonts, if you have them here
-v /home/$USER/.guix-profile/share/fonts/opentype:/usr/share/fonts/opentype-guix \ # ditto
 --shm-size 2g  --privileged someprefix/firefox  # substitute your prefix here for "someprefix"
```

Where again `someprefix` is whatever you chose before. `chmod +x
~/.local/bin/firefox` and then you can run Firefox with
`firefox`. (You'll have to make sure `dockerd` is running.)

Perhaps this could be useful elsewhere. It has the advantage of being
a nice [Alpine Linux](https://alpinelinux.org/) [musl-libc](https://www.musl-libc.org/)/[libressl](https://www.libressl.org/) build of Firefox running inside
your GuixSD (or whatever you're using).

What doesn't work:

-   Spellchecking - I don't quite know why, hunspell should be
    available. Just disable spellchecking for now (otherwise everything
    will be underlined in red).
-   [Wire](https://wire.com)'s web portal.
-   Saving or uploading from anywhere but `/tmp/Downloads` (but this is
    a feature really).
-   If you want CJK fonts, you'll have to install them in Nix or Guix
    (Alpine doesn't seem to have any); this is indicated in the Firefox
    launch script above.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
