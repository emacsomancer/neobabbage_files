+++
title = "Grab word etymologies in Emacs"
author = ["Benjamin Slade"]
date = 2018-08-26T21:49:00-06:00
tags = ["etymology"]
categories = ["emacs", "elisp"]
draft = false
creator = "Emacs 26.1 (Org mode 9.1.14 + ox-hugo)"
+++

On Xah Lee's [blog](http://ergoemacs.org/emacs/blog.html) I noticed an entry on linking to word etymologies
from Emacs (2018-08-16: "emacs, create link of word etymology"). What
his function does is create a html link to the [Etymonline.com](http://www.etymonline.com) page on
the currently selected word.

But I thought: it would be great to have a quick way of pulling up
etymological information within Emacs itself. So, with a little help
from [Links](http://links.twibright.com) (i.e. you'll need to have Links installed), here is a first
attempt at such a function:

```emacs-lisp
(defun slade/etymonbuf ()
  "Look up word etymology notes from http://www.etymonline.com. Requires [[http://links.twibright.com/][links] to be installed."
  (interactive)
  (let ($p1 $p2 $input $result)    ;; lifted from Xah Lee's 2018-08-16: "emacs, create link of word etymology"
    (if (use-region-p)
	(progn (setq $p1 (region-beginning))
	       (setq $p2 (region-end)))
      (progn (setq $p1 (line-beginning-position))
	     (setq $p2 (line-end-position))))
    (setq $input (buffer-substring-no-properties $p1 $p2)) ;;
    (generate-new-buffer $input)  ; generate new buffer titled with entry word
    (switch-to-buffer (last-buffer)) ; switch to that new buffer
    (insert (shell-command-to-string (concat "links -dump http://www.etymonline.com/word/" $input))) ; dump text via links from etymonline
    (goto-char (point-min)) ; go to beginning
    (if (re-search-forward "Error 404 (Not Found)" nil t)   ; for non-word or words with no etymonline entry
	(slade/word-not-found)   ; if 404, kill buffer and display minibuffer message
      (slade/trim-etymonbuf))))  ; otherwise trim off unneeded text from header and footer

(defun slade/trim-etymonbuf ()
  "Trim off unneeded text from top and bottom."
  (goto-char (point-min))		; goto beginning of buffer
  (search-forward "[\s\s]") 		; the text output of links always delivers text with a "[  ]" right before the text we want
  (let ((begdel (point)))
    (delete-region 1 begdel))		; delete from beginning of buffer until "[  ]"
  (goto-char (point-max))  		; goto end of buffer
  (search-backward-regexp "Share[[:space:]]*Share on FacebookShare")   ; the unneeded text at the bottom starts with a "Share" section
  (let ((enddel (point)))
    (delete-region (point-max) enddel))	; delete from end of buffer until Share section
  (goto-char (point-min)))		; move point back to beginning

(defun slade/word-not-found ()
  "Delete buffer and display minibuffer message."
  (kill-buffer)
  (message "Sorry, word not found."))
```

After `eval`'ing the above, one day, if you have an idle fancy to know
the etymology of a word, you can select it and call `slade/etymonbuf`
and have a quick peek at what Etymonline has to say.  Here's what you
get if you try this on "lisp":

```text
                                lisp (n.)

"act or habit of lisping," 1620s, from lisp (v.).

lisp (v.)

sometimes lipse, late 14c. alteration of wlisp, from late Old English
awlyspian "to lisp, to pronounce 's' and 'z' imperfectly," from wlisp
(adj.) "lisping," which is probably imitative (compare Middle Dutch, Old
High German lispen, Danish læspe, Swedish läspa). General sense "speak
imperfectly or childishly" is from 17c. Transitive sense from 1610s.
Related: Lisped; lisping. Suggestive of effeminacy from 14c.
```

Probably not the lisp you were looking for, but interesting none the
less.

Of course it would be nice to retain italics and not to have to depend
on an external application, so there's more that can be done.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
