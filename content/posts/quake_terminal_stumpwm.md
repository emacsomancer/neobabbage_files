+++
title = "Quake-style drop-down terminal in StumpWM"
author = ["Benjamin Slade"]
date = 2018-09-18T20:45:00-06:00
categories = ["lisp", "stumpwm"]
draft = false
creator = "Emacs 26.1 (Org mode 9.1.14 + ox-hugo)"
+++

One thing I've missed in StumpWM is a Quake-style drop-down terminal,
like what Guake provides (and I have a Lua-one in my AwesomeWM
config). It may be that I'm still haven't fully absorbed the
StumpWM-mindset and that I should be doing this a different way. But
up until now when I'm using StumpWM I've tended to end up with a heap
of terminal windows that are a pain to navigate through (I have a
`run-or-raise` command associated with xterm, but it starts from the
first xterm window and usually I want the last – something else to
figure out how to do). It's nice to have a 'working terminal' that one
can quickly summon and dismiss.

There's a number of issues for creating a Quake-type drop-down in
StumpWM (at least based on my current knowledge). The first is fairly
easily dealt with - I don't want more than one Quake terminal around.

`run-or-raise` will prevent multiple instances from being created:

```lisp
(defcommand urxvt () ()
  "Start an urxvt instance or switch to it, if it is already running."
  (run-or-raise "urxvt"'(:title "urxvt")))
```

(I'm cheating here a bit because I'm not quite sure how to handle
title-management. I know windows can be retitled, and I initially
tried setting the window title in the above function to "quake", but
somehow I ended up also sporadically re-titling **other** windows as
"quake", so I've side-stepped the whole issue by just using urxvt for
my drop-down terminal, with my regular terminal being xterm.)

What we want then is a function that toggles our Quake terminal
on-and-off: essentially if the current window is not urxvt, then call
the `urxvt` function defined above, and if the current window is urxvt
then switch back to the last used window before urxvt was called.  The
basic thing we need for this is `(if (equal (window-title
(current-window)) "urxvt")`.

The trouble is that StumpWM has a nice feature which is similar to
doing `C-x b RET` in Emacs: it essentially switches to the last window
used, so it can be a nice feature for switching back and forth between
two different windows you're working in. The function is
`pull-hidden-other` and it is invoked with "prefix prefix", which for
me is `s-f s-f` (Super+F, twice), but by default is `C-t C-` (Ctrl+t
twice). Why this creates a problem is because our Quake terminal will
muck that up because **it** will often end up being the last used
window, and that's not what I want. I want `pull-hidden-other` to
switch to the last used window that's **not** the Quake drop-down.

I tried a number of different things, and this may still not be the
best solution, but it works:

```lisp
(defcommand rxvt-quake () ()
  "Toggle rxvt-quake window."
  (if (equal (window-title (current-window)) "urxvt")   ; if the current window is quake
      (progn
	(other-window)    ; switch back to window quake was called from
	(select-window-by-number *real-other-window*)  ; switch to the 'real' "other-window"
	(other-window))  ; switch back to the original window - this way after quake finishes, the original configuration is restored
      (progn          ; otherwise, if the current window is NOT quake
      (other-window)   ; first switch the current "other-window"
	(if (not (equal (window-title (current-window)) "urxvt")) ; if the current
								  ; "other-window" is
								  ; quake itself, do
								  ; nothing
	    (setf *real-other-window* (window-number (current-window)))) ; otherwise store the window-number of the current other-window
	(other-window) ; switch back to the window originally called from
	(urxvt)))) ; run-or-raise urxvt
```

So this function indeed checks to see whether the current window is
our Quake drop-down terminal. If it's not, it first switches to the
currently last used window with `(other-window)` and stores the number
of this window in `*real-other-window*` (but **only** if this
last-used-window isn't itself urxvt - otherwise we'll sometime end up
'over-writing' the actual last-used-window we want to keep). It then
switches back to window that was active when `rxvt-quake` was invoked,
and then it calls the `urxvt` function which either launches a new
urxvt (if none currently exists), or raises the currently running
urxvt window. Storing the value in `*real-other-window*` gives us a
way to remember what the 'real' last-used window should be.

If `rxvt-quake` is invoked while the Quake urxvt drop-down is the
currently active window, then first `(other-window)` is invoked,
switching us back to the window that was active before we called the
drop-down terminal, then we switch to our stored window (which was the
'real' last-used window before Quake was invoked), and then we switch
back with `(other-window)` to the window that was active when Quake
was first invoked.

This way the window configuration that existed before we summoned our
Quake drop-down terminal is restored, and `pull-hidden-other`
effectively ignores the Quake drop-drop.

Bind this command to the traditional F12 with:

```lisp
(define-key *top-map* (kbd "F12") "rxvt-quake")
```

And you're got a 'traditional' Quake drop-down terminal in StumpWM.

It is a 'full-length' drop-down terminal, however. It might be nice to
have it be a fraction of the screen like a more traditional Quake drop-down.

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
