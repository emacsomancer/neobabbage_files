+++
title = "Dynamically set pdf-tools annotation colours"
author = ["Benjamin Slade"]
date = 2021-05-27T12:25:03-06:00
images = ["images/pdf-tools-highlightning.png"]
categories = ["emacs"]
draft = false
creator = "Emacs 28.0.50 (Org mode 9.4.6 + ox-hugo)"
+++

In Emacs, [`pdf-tools`](https://github.com/politza/pdf-tools) can be used to add annotations to a PDF
document. It can be useful to have multiple annotation colours though,
and be able to set these on the fly.

Here's an example of how to do it with four colours:

```elisp
;; annotation colours
(defun bms/pdf-annot-colour-blue ()
  (interactive)
  (setq pdf-annot-default-markup-annotation-properties
        '((label . "") (color . "blue") (popup-is-open)))
  (message "%s" (propertize "Annotation colour set to blue." 'face '(:foreground "blue"))))

(defun bms/pdf-annot-colour-yellow ()
  (interactive)
  (setq pdf-annot-default-markup-annotation-properties
        '((label . "") (color . "yellow") (popup-is-open)))
    (message "%s" (propertize "Annotation colour set to yellow." 'face '(:foreground "yellow"))))

(defun bms/pdf-annot-colour-red ()
  (interactive)
  (setq pdf-annot-default-markup-annotation-properties
        '((label . "") (color . "red") (popup-is-open)))
  (message "%s" (propertize "Annotation colour set to red." 'face '(:foreground "red"))))

(defun bms/pdf-annot-colour-orange ()
  (interactive)
  (setq pdf-annot-default-markup-annotation-properties
        '((label . "") (color . "orange") (popup-is-open)))
  (message "%s" (propertize "Annotation colour set to orange." 'face '(:foreground "orange"))))

;; rebind keys for pdf-tools
(defun bms/pdf-tools-mode-config ()
  "Set pdf-tools keybindings."
  (local-set-key (kbd "R") #'bms/pdf-annot-colour-red)
  (local-set-key (kbd "L") #'bms/pdf-annot-colour-blue)
  (local-set-key (kbd "O") #'bms/pdf-annot-colour-orange)
  (local-set-key (kbd "Y") #'bms/pdf-annot-colour-yellow))

;; add to pdf-view-mode-hook
(add-hook 'pdf-view-mode-hook #'bms/pdf-tools-mode-config)
```

Example in use (though some of the colours might be better chosen, e.g. a
lighter shade of blue):
![](/ox-hugo/pdf-tools-highlighting.png)

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
